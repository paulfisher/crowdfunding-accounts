<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    protected $table = 'account';

    protected $fillable = ['platform_key', 'platform_id', 'person_id'];


    public function person() {
        return $this->belongsTo('App\Person');
    }

    public function platform() {
        return $this->belongsTo('App\Platform');
    }

    public function url() {
        if ($this->platform->code == 'kickstarter') {
            if ($this->person->projects()->exists()) {
                return $this->person->projects()->first()->url . '/creator_bio';
            }
        }
        return strtr($this->platform->url, [
            '{platform_key}' => $this->platform_key
        ]);
    }
}
