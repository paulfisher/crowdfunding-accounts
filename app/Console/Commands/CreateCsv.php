<?php

namespace App\Console\Commands;

use App\Models\Blog;
use App\Models\BlogChannel;
use App\Models\Channel;
use App\Person;
use App\Services\Channels\ChannelImportService;
use Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CreateCsv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'csv:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import posts from channels.';

    public function __construct() {
        parent::__construct();
    }

    public function handle() {
        $csvPath = public_path() . '/csv/';

        $personsTotalCount = Person::count();

        $personsPerPart = 5000;
        if ($personsPerPart <= 0) {
            return;
        }

        $parts = intval(ceil($personsTotalCount/$personsPerPart));

        for ($part = 0; $part < $parts; $part++) {
            $fp = fopen($csvPath . 'part-' . $part . '.csv', 'w');

            fputcsv($fp, [
                'Name', 'FacebookId', 'Facebook url', 'Projects', 'Info'
            ]);
            foreach (Person::skip($part*$personsPerPart)->take($personsPerPart)->get() as $person) {
                fputcsv($fp, [
                    $person->name,
                    $person->facebook_key,
                    $person->facebookUrl(),
                    $person->projects_count,
                    url('person/' . $person->id)
                ]);
            }
            fclose($fp);
        }
    }
}
