<?php

namespace App\Http\Controllers;

use App\Http\Requests\AccountsRequest;
use App\Person;
use App\Platform;
use App\Project;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;

class PlatformController extends Controller {

    public function accounts(AccountsRequest $request) {
        $defaultPageSize = 20;
        if (!$request->pageSize) {
            $request->pageSize = $defaultPageSize;
        }
        $persons = $this->getPersonsByRequest($request)->paginate($request->pageSize);
        $platforms = Platform::all();

        return view('platform.accounts', compact('persons', 'platforms', 'platform', 'request'));
    }

    private function getPersonsByRequest(AccountsRequest $request) {
        $platform = $request->platform();
        $platformProjectsSortBy = 'projects_count';
        $platformProjectsSortAscDesc = $request->sort == 'projectsAsc' ? 'asc' : 'desc';
        if ($platform) {
            switch ($platform->code) {
                case "kickstarter":
                    $platformProjectsSortBy = 'kickstarter_projects_count';
                    break;
                case "indiegogo":
                    $platformProjectsSortBy = 'indiegogo_projects_count';
                    break;
                case "pozible":
                    $platformProjectsSortBy = 'pozible_projects_count';
                    break;
            }
        }
        return Person::orderBy($platformProjectsSortBy, $platformProjectsSortAscDesc);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {

    }

    public function index() {
        return view('platform.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Requests\CreateProjectRequest $request)
    {
        $platform = Platform::where('code', $request->platform)->firstOrFail();

        $fbKey = $request->get('person_facebook_key');
        if ($queryPos = strpos($fbKey, '?')) {
            $fbKey = substr($fbKey, 0, $queryPos);
        }

        $person = Person::where('facebook_key', $fbKey)->first();
        if (!$person) {
            $person = Person::create([
                'name' => $request->get('person_name'),
                'facebook_key' => $fbKey,
                'email' => $request->get('person_email'),
            ]);
            $person->accounts()->create([
                'platform_key' => $request->get('person_platform_key'),
                'platform_id' => $platform->id
            ]);
        } else {
            if (!in_array($request->get('person_platform_key'), $person->accounts->lists('platform_key')->toArray())) {
                $person->accounts()->create([
                    'platform_key' => $request->get('person_platform_key'),
                    'platform_id' => $platform->id
                ]);
            }
        }

        $project = Project::where('url', $request->get('project_url'))->first();
        if ($project) {
            if (!in_array($project->id, $person->projects->lists('id')->toArray())) {
                $person->projects()->attach($project->id);
            }
        } else {
            $person->projects()->create([
                'name' => $request->get('project_name'),
                'platform_id' => $platform->id,
                'url' => $request->get('project_url'),
            ]);

        }

        return 'OK';
    }

    public function viewPerson(Person $person) {
        return view('platform.person', compact('person'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function csv($which) {
        $filename = 'crowdfund-grabber-' . Carbon::now() . '.csv';

        header("Content-type: text/csv");
        header("Cache-Control: no-store, no-cache");
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        $persons = [];
        switch ($which) {
            case 'all':
                $persons = Person::last()->get();
                break;
            case 'lastMonth':
                $persons = Person::lastMonth()->last()->get();
                break;
        }

        return view('platform.csv', compact('persons'));
    }
}
