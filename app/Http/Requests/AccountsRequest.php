<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Platform;

/**
 * @property number platform
 */
class AccountsRequest extends Request {

    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            'platform' => 'integer',
        ];
    }

    /**
     * @return null|Platform
     */
    public function platform() {
        if (!$this->platform) {
            return null;
        }
        return Platform::find($this->platform);
    }
}
