<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateProjectRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_name' => 'required',
            'project_url' => 'required',
            'platform' => 'required',
            'person_name' => 'required',
            'person_platform_key' => 'required',
            'person_facebook_key' => 'required',
        ];
    }
}
