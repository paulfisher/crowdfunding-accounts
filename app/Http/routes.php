<?php


Route::group(['middleware' => 'auth.basic'], function () {
    Route::resource('/', 'PlatformController@index');
    Route::get('accounts', 'PlatformController@accounts');
    Route::get('accounts/fbIds', 'PlatformController@accountsFacebookIds');
    Route::get('contacts', function () {
        return view('contacts');
    });

    Route::get('platform/csv/{which}', 'PlatformController@csv');
    Route::resource('platform', 'PlatformController');

    Route::get('person/{person}', 'PlatformController@viewPerson');
});