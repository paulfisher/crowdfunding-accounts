<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Person extends Model
{
    protected $table = 'person';

    protected $fillable = ['name', 'facebook_key', 'email'];

    public function facebookUrl() {
        return "http://facebook.com/" . $this->facebook_key;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function projects() {
        return $this->belongsToMany('App\Project')->withTimestamps();
    }

    public function scopeLastMonth($query) {
        $query->where('created_at', '>=', Carbon::now()->addMonth(-1));
    }

    public function scopeLast($query) {
        return $query->orderBy('id', 'desc');
    }

    public function accounts() {
        return $this->hasMany('App\Account');
    }
}
