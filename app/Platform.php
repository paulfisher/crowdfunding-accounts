<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Platform extends Model
{
    protected $table = 'platform';

    protected $fillable = ['name', 'url', 'code'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function projects() {
        return $this->hasMany('App\Project');
    }

    public static function kickstarter() {
        return self::where('code', 'kickstarter')->first();
    }

    public static function indiegogo() {
        return self::where('code', 'indiegogo')->first();
    }

    public static function pozible() {
        return self::where('code', 'pozible')->first();
    }
}
