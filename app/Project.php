<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'project';

    protected $fillable = ['name', 'platform_id', 'url'];

    public function platform(){
        return $this->belongsTo('App\Platform');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function persons() {
        return $this->belongsToMany('App\Person');
    }
}
