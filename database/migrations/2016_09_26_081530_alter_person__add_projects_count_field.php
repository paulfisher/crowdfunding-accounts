<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterPersonAddProjectsCountField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('person', function (Blueprint $table) {
            $table->integer('projects_count')->nullable();
            $table->integer('kickstarter_projects_count')->nullable();
            $table->integer('indiegogo_projects_count')->nullable();
            $table->integer('pozible_projects_count')->nullable();
        });

        $kickstarterPlatform = \App\Platform::kickstarter();
        $indiegogoPlatform = \App\Platform::indiegogo();
        $poziblePlatform = \App\Platform::pozible();

        \App\Person::all()->each(function($person) use($kickstarterPlatform, $indiegogoPlatform, $poziblePlatform) {
            $person->projects_count = $person->projects()->count();
            $person->kickstarter_projects_count = $person->projects()->where('platform_id', $kickstarterPlatform->id)->count();
            $person->indiegogo_projects_count = $person->projects()->where('platform_id', $indiegogoPlatform->id)->count();
            $person->pozible_projects_count = $person->projects()->where('platform_id', $poziblePlatform->id)->count();
            $person->save();
        });

        \App\Project::where('platform_id', $kickstarterPlatform->id)->get()->each(function($project) {
            $project->name = strtr($project->name, ['/creator_bio' => '']);
            $project->url = strtr($project->url, ['/creator_bio' => '']);
            $project->save();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('person', function (Blueprint $table) {
            $table->dropColumn('projects_count');
            $table->dropColumn('kickstarter_projects_count');
            $table->dropColumn('indiegogo_projects_count');
            $table->dropColumn('pozible_projects_count');
        });
    }
}
