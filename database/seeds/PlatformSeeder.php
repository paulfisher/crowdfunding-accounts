<?php

use App\Platform;
use Illuminate\Database\Seeder;

class PlatformSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Platform::create([
            'name' => 'Kickstarter.com',
            'url' => 'https://www.kickstarter.com/profile/{platform_key}',
            'code' => 'kickstarter',
        ]);
        Platform::create([
            'name' => 'Indiegogo.com',
            'url' => 'https://www.indiegogo.com/individuals/{platform_key}',
            'code' => 'indiegogo',
        ]);
        Platform::create([
            'name' => 'Pozible.com',
            'url' => 'http://www.pozible.com/profile/{platform_key}',
            'code' => 'pozible',
        ]);
    }
}
