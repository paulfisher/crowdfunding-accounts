<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>CrowdFunding stat</title>
    {{--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link href='//fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>--}}
</head>
<body>
    <p>
        <a href="/">Home</a>
        <a href="/accounts">Accounts</a>
        <a href="/files/founders.zip" style="margin-left: 10px;">Download all</a>
    </p>

    <div>
        @yield('content')
    </div>

    <script src="//code.jquery.com/jquery.js"></script>
    {{--<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>--}}
</body>
</html>