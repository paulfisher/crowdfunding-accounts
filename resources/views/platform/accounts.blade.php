
@extends('app')

@section('content')

    <style>
        ul.pagination li {
            list-style: none;
            float: left;
            margin-right: 20px;
        }
    </style>


    <div>
        <form method="get" action="" style="border: 1px solid #eeeeee;padding: 10px;">
            <p>
                <label>
                    Platform:
                    <select name="platform">
                        <option value="">All</option>
                        @foreach ($platforms as $platform)
                            <option value="{{$platform->id}}" @if ($platform->id == $request->platform) selected="selected" @endif>{{$platform->name}}</option>
                        @endforeach
                    </select>
                </label>
            </p>
            <p>
                <label>
                    Sort by:
                    <select name="sort">
                        <option value="default" @if ($request->sort == 'default') selected="selected" @endif>Default</option>
                        <option value="projectsAsc" @if ($request->sort == 'projectsAsc') selected="selected" @endif>Projects ascend</option>
                        <option value="projectsDesc" @if ($request->sort == 'projectsDesc') selected="selected" @endif>Projects descend</option>
                    </select>
                </label>
            </p>

            <p>
                <label>
                    Page size:
                    <select name="pageSize">
                        <option value="20" @if ($request->pageSize == '20') selected="selected" @endif>20</option>
                        <option value="50" @if ($request->pageSize == '50') selected="selected" @endif>50</option>
                        <option value="100" @if ($request->pageSize == '100') selected="selected" @endif>100</option>
                        <option value="500" @if ($request->pageSize == '500') selected="selected" @endif>500</option>
                        <option value="1000" @if ($request->pageSize == '1000') selected="selected" @endif>1000</option>
                        <option value="5000" @if ($request->pageSize == '5000') selected="selected" @endif>5000</option>
                    </select>
                </label>
            </p>

            <p>
                <label>
                    Show only facebook ids
                    <input type="checkbox" name="onlyFacebookIds" @if ($request->onlyFacebookIds) checked="checked" @endif/>
                </label>
            </p>

            <input type="submit" value="search"/>
        </form>
    </div>

    @if ($request->onlyFacebookIds)

        @foreach ($persons as $person)
            {{$person->facebook_key}}<br/>
        @endforeach

        {!! $persons->appends(['sort' => $request->sort, 'platform' => $request->platform, 'onlyFacebookIds' => $request->onlyFacebookIds])->render() !!}
    @else
        @include('platform.partial.account-table', ['persons' => $persons, 'platform' => $request->platform])
    @endif


@endsection