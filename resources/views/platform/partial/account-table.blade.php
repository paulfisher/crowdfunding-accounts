<table>
    <thead>
    <tr>
        <th>Name</th>
        <th>Facebook</th>
        <th>Projects</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($persons as $person)
        <tr>
            <td>
                {!! link_to('person/' . $person->id, $person->name, ['target' => '_blank']) !!}
            </td>
            <td>
                {!! link_to($person->facebookUrl(), $person->facebookUrl(), ['target' => '_blank']) !!}
            </td>
            <td>
                @if ($platform == \App\Platform::kickstarter()->id)
                    {{ $person->kickstarter_projects_count }}
                @elseif ($platform == \App\Platform::indiegogo()->id)
                    {{ $person->indiegogo_projects_count }}
                @elseif ($platform == \App\Platform::pozible()->id)
                    {{ $person->pozible_projects_count }}
                @else
                    {{ $person->projects_count }}
                @endif
            </td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
    <tr>
        <td colspan="3">
            {!! $persons->appends(['sort' => $request->sort, 'platform' => $request->platform, 'onlyFacebookIds' => $request->onlyFacebookIds])->render() !!}
        </td>
    </tr>
    </tfoot>
</table>