
@extends('app')

@section('content')

<div>

    <img src="http://graph.facebook.com/{{$person->facebook_key}}/picture?type=large"/>
    <p>
        {{$person->name}}
    </p>
    <p>
        found {{$person->created_at->diffForHumans()}}
    </p>
    <p>
        {!! link_to($person->facebookUrl(), 'facebook', ['target' => '_blank']) !!}<br/>
        @foreach ($person->accounts as $account)
            {!! link_to($account->url(), $account->platform->code, ['target' => '_blank']) !!}<br/>
        @endforeach
    </p>
    <span>projects: {{$person->projects()->count()}}</span>

    <table>
        <thead>
        <tr>
            <th>Project</th>
            <th>Platform</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($person->projects as $project)
            <tr>
                <td>
                    {!! link_to($project->url, null, ['target' => '_blank']) !!}
                </td>
                <td>
                    {{ $project->platform->code }}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>


@endsection